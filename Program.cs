﻿using System;
using System.Threading;

namespace firstProj {

    class Program {
        static void Main(string[] args) {

            var videoEncode = new VideoEncoded(); // the class where the event is
            var mailservice = new MailService();
            videoEncode.videoEncoderEvent += mailservice.ONvideoEncoded;
            var smsservice = new SMSService();
            videoEncode.videoEncoderEvent += smsservice.ONvideoEncoded;
            videoEncode.Encode(new Video()
            {
                title = "iyiyiyi"
            });
            //
            Console.ReadKey();

        }

    }

    class MailService
    {
        public void ONvideoEncoded(object source, EventArgs e) {
            System.Console.WriteLine("mail notif");
        }
    }

     class SMSService
    {
        public void ONvideoEncoded(object source, EventArgs e) {
            System.Console.WriteLine("sms notif" );
        }
    }

    class Video
    {
        public String title { get; set; }
    }

    class VideoEncoded {
        // this is a delegate
        public delegate void VideoEncoderEventHundler(object item, EventArgs args);

        // this is a event based on the delegate
        public event VideoEncoderEventHundler videoEncoderEvent;

        // this is the event
        protected virtual void ONvideoEncoded() {
            // 
            Console.WriteLine("This is an event");
            // Console.w("this is an event");
            if (videoEncoderEvent != null)
                videoEncoderEvent(this, EventArgs.Empty);
        }

        public void Encode(Video video) {
            System.Console.WriteLine("Encodeing...");
            // Threed
            // Thread.Sleep(1000);
            ONvideoEncoded();
        }
    }

    // class VideoSubscriber
    // {
    //     // voi
    // }

}